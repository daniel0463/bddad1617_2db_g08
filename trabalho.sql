CREATE TABLE AVIAO(
serialNumber integer primary key
);

CREATE TABLE MODELO(
velocidadeMax number(4,2) primary key,
capacidadeDeposito number(4,2) NOT NULL,
maxCarga number(4,2) NOT NULL
);

CREATE TABLE MARCA(
codMarca number(9) primary key,
designacao varchar(44) NOT NULL
);

CREATE TABLE LUGAR(
codLugar number(9) primary key
);

CREATE TABLE CATEGORIA_LUGAR(
codLugar number(9) primary key
);

CREATE TABLE PRECO(
valorPreco number(4,2) primary key
);

CREATE TABLE PLANEAMENTO(
dataInicio char(10),
dataFim char(10),
horaInicio char(10),
horaFim char(10),  
CONSTRAINT uc_Planning PRIMARY KEY(dataInicio,dataFim,horaInicio,horaFim)
);

CREATE TABLE AEROPORTO(
codigoIATA number(9) primary key,
nomeAeroporto varchar(44) NOT NULL,
cidade varchar(44) NOT NULL,
pais varchar(44) NOT NULL,
latitude number(4,2) NOT NULL,
longitude number(4,2) NOT NULL
);

CREATE TABLE VOO(
codigoVoo number(9) primary key,
aeroportoOrigem varchar(44) NOT NULL,
aeroportoDestino varchar(44) NOT NULL,
distancia number(4) NOT NULL
);

CREATE TABLE VOO_REGULAR(
codVooRegular number(9) primary key,
dataVoo char(10) NOT NULL,
dataChegada char(10) NOT NULL,
horaPartida char(5) NOT NULL,
horaChegada char(5) NOT NULL
);

CREATE TABLE VIAGEM(
idViagem number(9),
dataChegada char(10),
dataPartida char(10),
horaPartida char(10),
horaChegada char(10),
CONSTRAINT uc_Viagem PRIMARY KEY(idViagem,dataChegada,dataPartida,horaPartida,horaChegada)
);

CREATE TABLE RESERVA(
idReserva number(9) primary key
);

CREATE TABLE TIPO_VOO(
designacaoTVoo varchar(44) primary key
);

CREATE TABLE CATEGORIA_TRIPULACAO(
designacaoCT varchar(44) primary key
);

CREATE TABLE REMUNERACAO(
salario number(4,2) primary key
);

CREATE TABLE PASSAGEIRO(
nDocumento char(9) primary key,
tipoDocumento varchar(21) primary key,
nome varchar(22) NOT NULL
);

CREATE TABLE TRIPULANTE(
idTripulante char(9) primary key
);

CREATE TABLE TIPO_TRIPULANTE(
designacaoT varchar(44) primary key
);

CREATE TABLE CATEGORIA_TRIPULANTE(
designacaoC varchar(44) primary key
);


CREATE TABLE TRIPULACAO_VOO(
codTripulacaoVoo number(9)
);